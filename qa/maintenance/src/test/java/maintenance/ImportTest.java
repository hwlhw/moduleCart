package maintenance;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.junit.Test;

public class ImportTest {
	@Test
	public void test() throws Exception {
		String featuresName = "plugins";
		String tableName = "product";
		File dir = new File("src/test/resources/META-INF/data/" + featuresName + "/data_xml");
		File xmlFile = new File(dir, tableName + ".xml");
		Utils.writeToTable(Utils.getConnection(), new FileInputStream(xmlFile));
		System.out.println("finished");
	}
}

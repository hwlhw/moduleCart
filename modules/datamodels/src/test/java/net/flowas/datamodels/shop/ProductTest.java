/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.flowas.datamodels.shop;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author liujh
 */
public class ProductTest {
    EntityManager em;
    public ProductTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        em = Persistence.createEntityManagerFactory("assessPU").createEntityManager();
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void test(){
        Product product=new Product();
        product.setCode("123");
        em.getTransaction().begin();
        em.persist(product);
        em.getTransaction().commit();
    }
       
}

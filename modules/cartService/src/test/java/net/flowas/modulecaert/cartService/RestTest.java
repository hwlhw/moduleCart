package net.flowas.modulecaert.cartService;

import net.flowas.modulecaert.testsupport.AbstractContainerTest;
import net.flowas.modulecart.domain.Category;
import net.flowas.modulecart.rest.CategoryEndpoint;

import org.junit.Assert;
import org.junit.Test;

public class RestTest extends AbstractContainerTest {
	@Test
	public void test() throws Exception {
		Category entity = new Category();
		entity.setName("fff");
		CategoryEndpoint  endpoint= getInterface(CategoryEndpoint.class);
		Category resultEntity=endpoint.create(entity);
		Assert.assertNotNull(resultEntity);
	}
}
